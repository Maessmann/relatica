# Relatica

A Flutter application for interfacing with the Friendica social network.

<img src="screenshots/v.0.1.0/windows/RelaticaFirstScreenshot.png" alt="Relatica v0.1.0 on Windows Screenshot" width="300px"/>

## Project Objectives

* Have a native app client on mobile (Apple and Android) and desktop (Linux, Mac, and Windows)
* Providing a simpler UX for people to interact with Friendica
* Providing a better low-bandwidth environment experience than the web-app version running in a browser
* Reduce server side loading in the new fediverse era by doing techniques like leveraging paging of comments, local caching, and lazy loading.
* Provide more intuitive mechanisms for adding things like Content Warning/Spoiler text and image ALT-text


## Current Status

The project is currently in an early-beta state. If you'd like to use it at this time please 
see the notes at [Relatica Beta Testing Program](beta-program.md). 

It is possible to install it now by following the [install instructions](install.md).

If you would like to contribute please see [this Developers Notes](developers.md) section.


## Community and Support

[Relatica Community Matrix Discussion Room](https://matrix.to/#/#relatica-community:myportal.social)

[Issue Tracker](https://gitlab.com/mysocialportal/relatica/-/issues)

### Things I could use help from community on:
* More coders and testers are always welcome
* CI/CD
* Packaging for Linux operating systems using Flatpak, Snap, etc. (or individual packages per operating system)
* UI/UX cleaning up
* Graphics design for logo, website art
* Documentation
* Spreading the word

## License
Relatica is licensed with the [Mozilla Public License 2.0 (MPL)](LICENSE) copyleft license.