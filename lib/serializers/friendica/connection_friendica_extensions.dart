import '../../models/connection.dart';

extension ConnectionFriendicaExtensions on Connection {
  static Connection fromJson(Map<String, dynamic> json) {
    final status = json['following'] == 'true'
        ? ConnectionStatus.youFollowThem
        : ConnectionStatus.none;
    final name = json['name'] ?? '';
    final id = json['id_str'] ?? '';
    final profileUrl = Uri.parse(json['url'] ?? '');
    final network = json['network'] ?? 'unkn';

    return Connection(
        status: status,
        name: name,
        id: id,
        profileUrl: profileUrl.toString(),
        network: network);
  }
}
