import '../../models/gallery_data.dart';

extension GalleryDataFriendicaExtensions on GalleryData {
  static GalleryData fromJson(Map<String, dynamic> json) => GalleryData(
      count: int.tryParse(json['count'].toString()) ?? -1,
      name: json['name'] ?? 'Unknown',
      created: DateTime.tryParse(json['created']) ?? DateTime(0));
}
