import 'package:logging/logging.dart';

import '../../globals.dart';
import '../../models/engagement_summary.dart';
import '../../models/link_data.dart';
import '../../models/location_data.dart';
import '../../models/media_attachment.dart';
import '../../models/timeline_entry.dart';
import '../../services/connections_manager.dart';
import '../../services/hashtag_service.dart';
import '../../utils/dateutils.dart';
import 'connection_mastodon_extensions.dart';
import 'hashtag_mastodon_extensions.dart';

final _logger = Logger('TimelineEntryMastodonExtensions');

extension TimelineEntryMastodonExtensions on TimelineEntry {
  static TimelineEntry fromJson(Map<String, dynamic> json) {
    final int timestamp = json.containsKey('created_at')
        ? OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(json['created_at'])
            .fold(
                onSuccess: (value) => value,
                onError: (error) {
                  _logger.severe("Couldn't read date time string: $error");
                  return 0;
                })
        : 0;
    final id = json['id'] ?? '';
    final youReshared = json['reblogged'] ?? false;
    final isPublic = json['visibility'] == 'public';
    final parentId = json['in_reply_to_id'] ?? '';
    final parentAuthor = json['in_reply_to_account_id'] ?? '';
    final parentAuthorId = json['in_reply_to_account_id'] ?? '';
    final body = json['content'] ?? '';
    final author = json['account']['display_name'];
    final authorId = json['account']['id'];
    const title = '';
    final spoilerText = json['spoiler_text'] ?? '';
    final externalLink = json['uri'] ?? '';
    final actualLocationData = LocationData();
    final modificationTimestamp = timestamp;
    final backdatedTimestamp = timestamp;
    final isFavorited = json['favourited'] ?? false;
    final linkData = json['card'] == null
        ? <LinkData>[]
        : [LinkData.fromMastodonJson(json['card'])];
    final mediaAttachments = (json['media_attachments'] as List<dynamic>? ?? [])
        .map((json) => MediaAttachment.fromMastodonJson(json))
        .toList();
    final favoritesCount = json['favourites_count'] ?? 0;
    final repliesCount = json['replies_count'] ?? 0;
    final rebloggedCount = json['reblogs_count'] ?? 0;
    final engagementSummary = EngagementSummary(
      favoritesCount: favoritesCount,
      rebloggedCount: rebloggedCount,
      repliesCount: repliesCount,
    );

    final connectionManager = getIt<ConnectionsManager>();
    final connection = ConnectionMastodonExtensions.fromJson(json['account']);
    connectionManager.addConnection(connection);

    late final String reshareAuthor;
    late final String reshareAuthorId;
    if (json['reblog'] != null) {
      final rebloggedUser =
          ConnectionMastodonExtensions.fromJson(json['reblog']['account']);
      connectionManager.addConnection(rebloggedUser);
      reshareAuthor = rebloggedUser.name;
      reshareAuthorId = rebloggedUser.id;
    } else {
      reshareAuthorId = '';
      reshareAuthor = '';
    }

    final List<dynamic>? tags = json['tags'];
    if (tags?.isNotEmpty ?? false) {
      final tagManager = getIt<HashtagService>();
      for (final tagJson in tags!) {
        final tag = HashtagMastodonExtensions.fromJson(tagJson);
        tagManager.add(tag);
      }
    }

    return TimelineEntry(
      creationTimestamp: timestamp,
      modificationTimestamp: modificationTimestamp,
      backdatedTimestamp: backdatedTimestamp,
      locationData: actualLocationData,
      spoilerText: spoilerText,
      body: body,
      youReshared: youReshared,
      isPublic: isPublic,
      id: id,
      parentId: parentId,
      parentAuthorId: parentAuthorId,
      reshareAuthor: reshareAuthor,
      reshareAuthorId: reshareAuthorId,
      isFavorited: isFavorited,
      externalLink: externalLink,
      author: author,
      authorId: authorId,
      parentAuthor: parentAuthor,
      title: title,
      links: linkData,
      mediaAttachments: mediaAttachments,
      engagementSummary: engagementSummary,
    );
  }
}
