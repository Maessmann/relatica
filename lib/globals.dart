import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:uuid/uuid.dart';

final getIt = GetIt.instance;

String randomId() => const Uuid().v4().toString();

final platformHasCamera = Platform.isIOS || Platform.isAndroid;

final useImagePicker = kIsWeb || Platform.isAndroid || Platform.isIOS;

final useVideoPlayer = kIsWeb || Platform.isAndroid || Platform.isIOS;

Future<bool?> showConfirmDialog(BuildContext context, String caption) {
  return showDialog<bool>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(caption),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('OK'),
            onPressed: () {
              Navigator.pop(context, true); // showDialog() returns true
            },
          ),
        ],
      );
    },
  );
}

Future<bool?> showYesNoDialog(BuildContext context, String caption) {
  return showDialog<bool>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(caption),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('Yes'),
            onPressed: () {
              Navigator.pop(context, true); // showDialog() returns true
            },
          ),
          ElevatedButton(
            child: const Text('No'),
            onPressed: () {
              Navigator.pop(context, false); // showDialog() returns false
            },
          ),
        ],
      );
    },
  );
}
