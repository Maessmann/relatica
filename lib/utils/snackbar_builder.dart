import 'package:flutter/material.dart';

Future<void> buildSnackbar(BuildContext context, String message,
    {int durationSec = 3}) async {
  final snackBar = SnackBar(
      content: SelectableText(message),
      duration: Duration(seconds: durationSec),
      action: SnackBarAction(
        label: 'Dismiss',
        onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
      ));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
