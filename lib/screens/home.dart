import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../controls/app_bottom_nav_bar.dart';
import '../controls/padding.dart';
import '../controls/timeline/timeline_panel.dart';
import '../models/TimelineIdentifiers.dart';
import '../models/group_data.dart';
import '../services/timeline_manager.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _logger = Logger('$HomeScreen');

  final postText = TextEditingController();
  var currentType = TimelineType.home;
  GroupData? currentGroup;
  final types = [
    TimelineType.self,
    TimelineType.home,
    TimelineType.global,
    TimelineType.local,
    TimelineType.group,
  ];

  @override
  Widget build(BuildContext context) {
    _logger.finest('Build');
    final groups = context
        .watch<TimelineManager>()
        .getGroups()
        .getValueOrElse(() => [])
        .toList();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            DropdownButton<TimelineType>(
                value: currentType,
                items: types
                    .map((e) => DropdownMenuItem<TimelineType>(
                          value: e,
                          child: Text(e.toLabel()),
                        ))
                    .toList(),
                onChanged: (value) {
                  setState(() {
                    currentType = value!;
                  });
                }),
            const HorizontalPadding(),
            if (currentType == TimelineType.group)
              DropdownButton<GroupData>(
                  value: currentGroup,
                  items: groups
                      .map((g) => DropdownMenuItem<GroupData>(
                            value: g,
                            child: Text(g.name),
                          ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      currentGroup = value;
                    });
                  }),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {
              context.push('/post/new');
            },
            icon: Icon(
              Icons.edit,
              color: Theme.of(context).textTheme.bodyText1!.color,
            ),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
              child: TimelinePanel(
            timeline: TimelineIdentifiers(
              timeline: currentType,
              auxData: currentGroup?.id ?? '',
            ),
          )),
        ],
      ),
      bottomNavigationBar: const AppBottomNavBar(
        currentButton: NavBarButtons.timelines,
      ),
    );
  }
}
