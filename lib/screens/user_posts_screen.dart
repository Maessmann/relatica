import 'package:flutter/material.dart';

import '../controls/timeline/timeline_panel.dart';
import '../models/TimelineIdentifiers.dart';
import '../routes.dart';

class UserPostsScreen extends StatelessWidget {
  final String userId;

  const UserPostsScreen({super.key, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('User Posts'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).popUntil((route) {
                return route.settings.name == ScreenPaths.timelines;
              });
            },
            icon: const Icon(Icons.home),
          ),
        ],
      ),
      body: Center(
        child: TimelinePanel(
          timeline: TimelineIdentifiers.profile(userId),
        ),
      ),
    );
  }
}
