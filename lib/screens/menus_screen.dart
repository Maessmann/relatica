import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../controls/app_bottom_nav_bar.dart';
import '../globals.dart';
import '../routes.dart';
import '../services/auth_service.dart';

class MenusScreen extends StatelessWidget {
  static const menuButtonWidth = 350.0;
  static const menuButtonHeight = 125.0;

  @override
  Widget build(BuildContext context) {
    final menuItems = [
      buildMenuButton('Gallery', () => context.pushNamed(ScreenPaths.gallery)),
      buildMenuButton('Profile', () => context.pushNamed(ScreenPaths.profile)),
      buildMenuButton(
          'Settings', () => context.pushNamed(ScreenPaths.settings)),
      buildMenuButton('Logout', () async {
        final confirm = await showYesNoDialog(context, 'Log out account?');
        if (confirm == true) {
          await getIt<AuthService>().signOut();
        }
      }),
    ];
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              mainAxisExtent: menuButtonHeight,
              maxCrossAxisExtent: menuButtonWidth,
            ),
            children: menuItems,
          ),
        ),
      ),
      bottomNavigationBar: const AppBottomNavBar(
        currentButton: NavBarButtons.menu,
      ),
    );
  }

  Widget buildMenuButton(String title, Function() onPressed) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(title),
      ),
    );
  }
}
