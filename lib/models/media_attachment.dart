import 'package:path/path.dart' as p;

import '../globals.dart';
import 'attachment_media_type_enum.dart';

class MediaAttachment {
  static final _graphicsExtensions = ['jpg', 'png', 'gif', 'tif'];
  static final _movieExtensions = ['avi', 'mp4', 'mpg', 'wmv'];

  final Uri uri;

  final int creationTimestamp;

  final Map<String, String> metadata;

  final AttachmentMediaType explicitType;

  final Uri thumbnailUri;

  final Uri fullFileUri;

  final String title;

  final String description;

  MediaAttachment(
      {required this.uri,
      required this.creationTimestamp,
      required this.metadata,
      required this.thumbnailUri,
      required this.fullFileUri,
      required this.title,
      required this.explicitType,
      required this.description});

  MediaAttachment.randomBuilt()
      : uri = Uri.parse('http://localhost/${randomId()}'),
        creationTimestamp = DateTime.now().millisecondsSinceEpoch,
        fullFileUri = Uri.parse(''),
        title = 'Random title ${randomId()}',
        thumbnailUri = Uri.parse('${randomId()}.jpg'),
        description = 'Random description ${randomId()}',
        explicitType = AttachmentMediaType.image,
        metadata = {'value1': randomId(), 'value2': randomId()};

  MediaAttachment.blank()
      : uri = Uri(),
        creationTimestamp = 0,
        thumbnailUri = Uri(),
        explicitType = AttachmentMediaType.unknown,
        title = '',
        fullFileUri = Uri(),
        description = '',
        metadata = {};

  factory MediaAttachment.fromMastodonJson(Map<String, dynamic> json) =>
      MediaAttachment(
          uri: Uri.parse(json['url'] ?? 'http://localhost'),
          creationTimestamp: 0,
          metadata: {},
          thumbnailUri: Uri.parse(json['url'] ?? 'http://localhost'),
          title: '',
          fullFileUri: Uri.parse(json['remote_url'] ?? 'http://localhost'),
          explicitType: AttachmentMediaType.parse(json['type']),
          description: json['description'] ?? '');

  @override
  String toString() {
    return 'FriendicaMediaAttachment{uri: $uri, creationTimestamp: $creationTimestamp, type: $explicitType, metadata: $metadata, title: $title, description: $description}';
  }

  Map<String, dynamic> toJson() => {
        'uri': uri.toString(),
        'creationTimestamp': creationTimestamp,
        'metadata': metadata,
        'type': explicitType,
        'thumbnailUri': thumbnailUri.toString(),
        'title': title,
        'description': description,
      };

  static AttachmentMediaType mediaTypeFromString(String path) {
    final extension = p.extension(path);

    if (_graphicsExtensions.contains(extension)) {
      return AttachmentMediaType.image;
    }

    if (_movieExtensions.contains(extension)) {
      return AttachmentMediaType.video;
    }

    return AttachmentMediaType.unknown;
  }
}
