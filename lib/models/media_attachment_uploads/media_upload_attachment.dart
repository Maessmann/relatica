import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MediaUploadAttachment {
  final String localFilePath;

  final String remoteUrl;

  final bool isExistingServerItem;

  String description;

  String remoteFilename;

  MediaUploadAttachment({
    this.localFilePath = '',
    this.remoteUrl = '',
    this.isExistingServerItem = false,
    this.description = '',
    this.remoteFilename = '',
  });

  factory MediaUploadAttachment.newItem(String localFilename) =>
      MediaUploadAttachment(
        localFilePath: localFilename,
        isExistingServerItem: false,
        remoteFilename: '',
      );

  factory MediaUploadAttachment.existingItem(String remoteUrl) =>
      MediaUploadAttachment(
        remoteUrl: remoteUrl,
        isExistingServerItem: true,
      );

  Widget getPreviewImage() {
    if (isExistingServerItem) {
      return CachedNetworkImage(imageUrl: remoteUrl);
    }

    return Image.file(File(localFilePath));
  }

  @override
  String toString() {
    return 'MediaUploadAttachment{localFilename: $localFilePath, remoteUrl: $remoteUrl, isExistingServerItem: $isExistingServerItem, description: $description}';
  }
}
