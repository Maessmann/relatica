import 'package:result_monad/result_monad.dart';

import 'exec_error.dart';

class Credentials {
  final String username;
  final String password;
  final String serverName;

  Credentials(
      {required this.username,
      required this.password,
      required this.serverName});

  factory Credentials.empty() => Credentials(
        username: '',
        password: '',
        serverName: '',
      );

  bool get isEmpty =>
      username.isEmpty && password.isEmpty && serverName.isEmpty;

  String get handle => '$username@$serverName';

  Credentials copy({String? username, String? password, String? serverName}) {
    return Credentials(
        username: username ?? this.username,
        password: password ?? this.password,
        serverName: serverName ?? this.serverName);
  }

  static Result<Credentials, ExecError> buildFromHandle(
      String handle, String password) {
    final elements = handle.split('@');
    if (elements.length != 2) {
      return Result.error(ExecError(
          type: ErrorType.authentication,
          message: 'Handle has invalid format: $handle'));
    }
    final result = Credentials(
      username: elements[0],
      password: password,
      serverName: elements[1],
    );

    return Result.ok(result);
  }

  @override
  String toString() {
    return 'Credentials{username: $username, password?: ${password.isNotEmpty}, serverName: $serverName}';
  }
}
