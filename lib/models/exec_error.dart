import 'package:result_monad/result_monad.dart';

Result<T, ExecError> buildErrorResult<T>({
  required ErrorType type,
  String message = '',
}) =>
    Result.error(
      ExecError(
        type: type,
        message: message,
      ),
    );

class ExecError {
  final ErrorType type;
  final String message;

  ExecError({required this.type, this.message = ''});

  ExecError copy({
    ErrorType? type,
    String? message,
  }) =>
      ExecError(
        type: type ?? this.type,
        message: message ?? this.message,
      );

  @override
  String toString() {
    return 'ExecError{type: $type, message: $message}';
  }
}

enum ErrorType {
  authentication,
  localError,
  missingEndpoint,
  notFound,
  parsingError,
}

extension ExecErrorExtension<T, E> on Result<T, E> {
  Result<T, ExecError> execErrorCast() => mapError((error) => error is ExecError
      ? error
      : ExecError(type: ErrorType.localError, message: error.toString()));

  FutureResult<T, ExecError> execErrorCastAsync() async => execErrorCast();
}
