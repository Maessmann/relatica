class GalleryData {
  final int count;
  final String name;
  final DateTime created;

  GalleryData({required this.count, required this.name, required this.created});
}
