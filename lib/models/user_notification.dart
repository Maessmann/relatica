import 'package:logging/logging.dart';

final _logger = Logger('NotificationType');

enum NotificationType {
  favourite,
  follow,
  follow_request,
  mention,
  reshare,
  reblog,
  status,
  unknown;

  String toVerb() {
    switch (this) {
      case NotificationType.favourite:
        return 'favorited';
      case NotificationType.follow:
        return 'follows';
      case NotificationType.follow_request:
        return 'sent follow request to you';
      case NotificationType.mention:
        return 'mentioned you';
      case NotificationType.reshare:
      case NotificationType.reblog:
        return 'reshared';
      case NotificationType.status:
        return 'updated';
      case NotificationType.unknown:
        return 'unknowned';
    }
  }

  static NotificationType parse(String? text) {
    if (text == null) {
      return unknown;
    }

    return NotificationType.values.firstWhere(
      (e) => e.name == text,
      orElse: () {
        _logger.severe('Parsing error, unknown type string: $text');
        return unknown;
      },
    );
  }
}

class UserNotification {
  final String id;
  final NotificationType type;
  final String fromId;
  final String fromName;
  final String fromUrl;
  final int timestamp;
  final String iid;
  final bool dismissed;
  final String content;
  final String link;

  UserNotification({
    required this.id,
    required this.type,
    required this.fromId,
    required this.fromName,
    required this.fromUrl,
    required this.timestamp,
    required this.iid,
    required this.dismissed,
    required this.content,
    required this.link,
  });

  @override
  String toString() {
    return 'UserNotification{id: $id, seen: $dismissed, fromName: $fromName, content: $content}';
  }
}
