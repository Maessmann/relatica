import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import 'data/interfaces/connections_repo_intf.dart';
import 'data/interfaces/groups_repo.intf.dart';
import 'data/interfaces/hashtag_repo_intf.dart';
import 'data/memory/memory_groups_repo.dart';
import 'data/objectbox/objectbox_cache.dart';
import 'data/objectbox/objectbox_connections_repo.dart';
import 'data/objectbox/objectbox_hashtag_repo.dart';
import 'globals.dart';
import 'models/TimelineIdentifiers.dart';
import 'services/auth_service.dart';
import 'services/connections_manager.dart';
import 'services/entry_manager_service.dart';
import 'services/gallery_service.dart';
import 'services/hashtag_service.dart';
import 'services/media_upload_attachment_helper.dart';
import 'services/notifications_manager.dart';
import 'services/secrets_service.dart';
import 'services/setting_service.dart';
import 'services/timeline_manager.dart';

final _logger = Logger('DI_Init');

Future<void> dependencyInjectionInitialization() async {
  final authService = AuthService();
  final secretsService = SecretsService();
  final entryManagerService = EntryManagerService();
  final timelineManager = TimelineManager();
  final galleryService = GalleryService();

  final service = SettingsService();
  await service.initialize();
  getIt.registerSingleton<SettingsService>(service);

  final objectBoxCache = await ObjectBoxCache.create();
  getIt.registerSingleton<ObjectBoxCache>(objectBoxCache);
  getIt.registerSingleton<IConnectionsRepo>(ObjectBoxConnectionsRepo());
  getIt.registerSingleton<IHashtagRepo>(ObjectBoxHashtagRepo());
  getIt.registerSingleton<IGroupsRepo>(MemoryGroupsRepo());
  getIt.registerLazySingleton<ConnectionsManager>(() => ConnectionsManager());
  getIt.registerLazySingleton<HashtagService>(() => HashtagService());
  getIt.registerSingleton(galleryService);
  getIt.registerSingleton<EntryManagerService>(entryManagerService);
  getIt.registerSingleton<SecretsService>(secretsService);
  getIt.registerSingleton<AuthService>(authService);
  getIt.registerSingleton<TimelineManager>(timelineManager);
  getIt.registerLazySingleton<MediaUploadAttachmentHelper>(
      () => MediaUploadAttachmentHelper());
  getIt.registerLazySingleton<NotificationsManager>(
      () => NotificationsManager());
  galleryService.getGalleries();

  await secretsService.initialize().andThenSuccessAsync((credentials) async {
    if (credentials.isEmpty) {
      return;
    }

    final wasLoggedIn = await authService.getStoredLoginState();
    if (wasLoggedIn) {
      final result = await authService.signIn(credentials);
      if (result.isSuccess) {
        timelineManager.updateTimeline(
            TimelineIdentifiers.home(), TimelineRefreshType.loadOlder);
      }
    } else {
      _logger.severe('Was not logged in');
    }
  });
}
