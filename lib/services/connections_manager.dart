import 'dart:collection';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import '../data/interfaces/connections_repo_intf.dart';
import '../data/interfaces/groups_repo.intf.dart';
import '../globals.dart';
import '../models/connection.dart';
import '../models/exec_error.dart';
import '../models/group_data.dart';
import 'auth_service.dart';

class ConnectionsManager extends ChangeNotifier {
  static final _logger = Logger('$ConnectionsManager');
  late final IConnectionsRepo conRepo;
  late final IGroupsRepo groupsRepo;

  ConnectionsManager() {
    conRepo = getIt<IConnectionsRepo>();
    groupsRepo = getIt<IGroupsRepo>();
  }

  bool addConnection(Connection connection) {
    return conRepo.addConnection(connection);
  }

  List<Connection> getKnownUsersByName(String name) {
    return conRepo.getKnownUsersByName(name);
  }

  bool updateConnection(Connection connection) {
    return conRepo.updateConnection(connection);
  }

  bool addAllConnections(Iterable<Connection> newConnections) {
    return conRepo.addAllConnections(newConnections);
  }

  Future<void> acceptFollowRequest(Connection connection) async {
    _logger.finest(
        'Attempting to accept follow request ${connection.name}: ${connection.status}');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.acceptFollow(connection))
        .match(
      onSuccess: (update) {
        _logger
            .finest('Successfully followed ${update.name}: ${update.status}');
        updateConnection(update);
        notifyListeners();
      },
      onError: (error) {
        _logger.severe('Error following ${connection.name}');
      },
    );
  }

  Future<void> rejectFollowRequest(Connection connection) async {
    _logger.finest(
        'Attempting to accept follow request ${connection.name}: ${connection.status}');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.rejectFollow(connection))
        .match(
      onSuccess: (update) {
        _logger
            .finest('Successfully followed ${update.name}: ${update.status}');
        updateConnection(update);
        notifyListeners();
      },
      onError: (error) {
        _logger.severe('Error following ${connection.name}');
      },
    );
  }

  Future<void> ignoreFollowRequest(Connection connection) async {
    _logger.finest(
        'Attempting to accept follow request ${connection.name}: ${connection.status}');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.ignoreFollow(connection))
        .match(
      onSuccess: (update) {
        _logger
            .finest('Successfully followed ${update.name}: ${update.status}');
        updateConnection(update);
        notifyListeners();
      },
      onError: (error) {
        _logger.severe('Error following ${connection.name}');
      },
    );
  }

  Future<void> follow(Connection connection) async {
    _logger.finest(
        'Attempting to follow ${connection.name}: ${connection.status}');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.followConnection(connection))
        .match(
      onSuccess: (update) {
        _logger
            .finest('Successfully followed ${update.name}: ${update.status}');
        updateConnection(update);
        notifyListeners();
      },
      onError: (error) {
        _logger.severe('Error following ${connection.name}');
      },
    );
  }

  Future<void> unfollow(Connection connection) async {
    _logger.finest(
        'Attempting to unfollow ${connection.name}: ${connection.status}');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.unFollowConnection(connection))
        .match(
      onSuccess: (update) {
        _logger
            .finest('Successfully unfollowed ${update.name}: ${update.status}');
        updateConnection(update);
        notifyListeners();
      },
      onError: (error) {
        _logger.severe('Error following ${connection.name}');
      },
    );
  }

  List<Connection> getMyContacts() {
    return conRepo.getMyContacts();
  }

  Future<void> updateAllContacts() async {
    _logger.fine('Updating all contacts');
    final clientResult = getIt<AuthService>().currentClient;
    if (clientResult.isFailure) {
      _logger.severe(
          'Unable to update contacts due to client error: ${clientResult.error}');
      return;
    }
    final client = clientResult.value;
    final results = <String, Connection>{};
    var moreResults = true;
    var maxId = -1;
    const limit = 1000;
    while (moreResults) {
      await client.getMyFollowers(sinceId: maxId, limit: limit).match(
          onSuccess: (followers) {
        for (final f in followers) {
          results[f.id] = f.copy(status: ConnectionStatus.theyFollowYou);
          int id = int.parse(f.id);
          maxId = max(maxId, id);
        }
        moreResults = followers.length >= limit;
      }, onError: (error) {
        _logger.severe('Error getting followers data: $error');
      });
    }

    moreResults = true;
    maxId = -1;
    while (moreResults) {
      await client.getMyFollowing(sinceId: maxId, limit: limit).match(
          onSuccess: (following) {
        for (final f in following) {
          if (results.containsKey(f.id)) {
            results[f.id] = f.copy(status: ConnectionStatus.mutual);
          } else {
            results[f.id] = f.copy(status: ConnectionStatus.youFollowThem);
          }
          int id = int.parse(f.id);
          maxId = max(maxId, id);
        }
        moreResults = following.length >= limit;
      }, onError: (error) {
        _logger.severe('Error getting followers data: $error');
      });
    }

    addAllConnections(results.values);
    final myContacts = conRepo.getMyContacts().toList();
    myContacts.sort((c1, c2) => c1.name.compareTo(c2.name));
    _logger.finest('# Contacts:${myContacts.length}');
    notifyListeners();
  }

  List<GroupData> getMyGroups() {
    final myGroups = groupsRepo.getMyGroups();
    if (myGroups.isEmpty) {
      _updateMyGroups(true);
    }

    return myGroups;
  }

  Result<List<GroupData>, ExecError> getGroupsForUser(String id) {
    final result = groupsRepo.getGroupsForUser(id);
    if (result.isSuccess) {
      return result;
    }

    if (result.isFailure && result.error.type != ErrorType.notFound) {
      return result;
    }

    _refreshGroupListData(id, true);
    return Result.ok(UnmodifiableListView([]));
  }

  FutureResult<bool, ExecError> addUserToGroup(
      GroupData group, Connection connection) async {
    _logger.finest('Adding ${connection.name} to group: ${group.name}');
    final result = await getIt<AuthService>().currentClient.andThenAsync(
        (client) => client.addConnectionToGroup(group, connection));
    result.match(
      onSuccess: (_) => _refreshGroupListData(connection.id, true),
      onError: (error) {
        _logger
            .severe('Error adding ${connection.name} to group: ${group.name}');
      },
    );

    return result.execErrorCast();
  }

  FutureResult<bool, ExecError> removeUserFromGroup(
      GroupData group, Connection connection) async {
    _logger.finest('Removing ${connection.name} from group: ${group.name}');
    final result = await getIt<AuthService>().currentClient.andThenAsync(
        (client) => client.removeConnectionFromGroup(group, connection));
    result.match(
      onSuccess: (_) => _refreshGroupListData(connection.id, true),
      onError: (error) {
        _logger.severe(
            'Error removing ${connection.name} from group: ${group.name}');
      },
    );

    return result.execErrorCast();
  }

  Result<Connection, ExecError> getById(String id) {
    return conRepo.getById(id).andThenSuccess((c) {
      if (c.status == ConnectionStatus.unknown) {
        _refreshConnection(c, true);
      }
      return c;
    }).execErrorCast();
  }

  Result<Connection, ExecError> getByName(String name) {
    return conRepo.getByName(name).andThenSuccess((c) {
      if (c.status == ConnectionStatus.unknown) {
        _refreshConnection(c, true);
      }
      return c;
    }).execErrorCast();
  }

  Future<void> fullRefresh(Connection connection) async {
    await _updateMyGroups(false);
    await _refreshGroupListData(connection.id, false);
    await _refreshConnection(connection, false);
    notifyListeners();
  }

  Future<void> _refreshGroupListData(String id, bool withNotification) async {
    _logger.finest('Refreshing member list data for Connection $id');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.getMemberGroupsForConnection(id))
        .match(
      onSuccess: (groups) {
        groupsRepo.updateConnectionGroupData(id, groups);
        if (withNotification) {
          notifyListeners();
        }
      },
      onError: (error) {
        _logger.severe('Error getting list data for $id: $error');
      },
    );
  }

  Future<void> _refreshConnection(
      Connection connection, bool withNotification) async {
    _logger.finest('Refreshing connection data for ${connection.name}');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.getConnectionWithStatus(connection))
        .match(
      onSuccess: (update) {
        updateConnection(update);
        if (withNotification) {
          notifyListeners();
        }
      },
      onError: (error) {
        _logger.severe('Error getting updates for ${connection.name}: $error');
      },
    );
  }

  Future<void> _updateMyGroups(bool withNotification) async {
    _logger.finest('Refreshing my groups list');
    await getIt<AuthService>()
        .currentClient
        .andThenAsync((client) => client.getGroups())
        .match(
      onSuccess: (groups) {
        _logger.finest('Got updated groups:${groups.map((e) => e.name)}');
        groupsRepo.clearGroups();
        groupsRepo.addAllGroups(groups);
        if (withNotification) {
          notifyListeners();
        }
      },
      onError: (error) {
        _logger.severe('Error getting my groups: $error');
      },
    );
  }
}
