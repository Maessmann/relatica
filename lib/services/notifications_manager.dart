import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import '../globals.dart';
import '../models/exec_error.dart';
import '../models/user_notification.dart';
import 'auth_service.dart';

class NotificationsManager extends ChangeNotifier {
  static final _logger = Logger('NotificationManager');
  final _notifications = <String, UserNotification>{};

  List<UserNotification> get notifications {
    final result = List<UserNotification>.from(_notifications.values);
    result.sort((n1, n2) {
      if (n1.dismissed == n2.dismissed) {
        return n2.timestamp.compareTo(n1.timestamp);
      }

      if (n1.dismissed && !n2.dismissed) {
        return 1;
      }

      return -1;
    });

    return result;
  }

  FutureResult<List<UserNotification>, ExecError> updateNotifications() async {
    final auth = getIt<AuthService>();
    final clientResult = auth.currentClient;
    if (clientResult.isFailure) {
      _logger.severe('Error getting Friendica client: ${clientResult.error}');
      return clientResult.errorCast();
    }

    final client = clientResult.value;
    final result = await client.getNotifications();
    if (result.isFailure) {
      return result.errorCast();
    }

    for (final n in result.value) {
      _notifications[n.id] = n;
    }

    notifyListeners();
    return Result.ok(notifications);
  }

  FutureResult<bool, ExecError> markSeen(UserNotification notification) async {
    final auth = getIt<AuthService>();
    final clientResult = auth.currentClient;
    if (clientResult.isFailure) {
      _logger.severe('Error getting Friendica client: ${clientResult.error}');
      return clientResult.errorCast();
    }

    final client = clientResult.value;
    final result = await client.clearNotification(notification);
    if (result.isSuccess) {
      notifyListeners();
    }

    updateNotifications();
    return result;
  }

  FutureResult<List<UserNotification>, ExecError> markAllAsRead() async {
    final auth = getIt<AuthService>();
    final clientResult = auth.currentClient;
    if (clientResult.isFailure) {
      _logger.severe('Error getting Friendica client: ${clientResult.error}');
      return clientResult.errorCast();
    }

    final client = clientResult.value;
    final result = await client.clearNotifications();
    if (result.isFailure) {
      return result.errorCast();
    }
    notifyListeners();
    return updateNotifications();
  }
}
