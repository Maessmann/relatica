import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:result_monad/result_monad.dart';

import '../globals.dart';
import '../models/exec_error.dart';
import '../models/gallery_data.dart';
import '../models/image_entry.dart';
import 'auth_service.dart';

class GalleryService extends ChangeNotifier {
  static final _logger = Logger('$GalleryService');
  final _galleries = <String, GalleryData>{};
  final _images = <String, Set<ImageEntry>>{};
  var _loaded = false;

  bool get loaded => _loaded;

  List<GalleryData> getGalleries() {
    if (_galleries.isEmpty) {
      updateGalleries();
    }

    return _galleries.values.toList(growable: false);
  }

  FutureResult<List<GalleryData>, ExecError> updateGalleries() async {
    final auth = getIt<AuthService>();
    final clientResult = auth.currentClient;
    if (clientResult.isFailure) {
      _logger.severe('Error getting Friendica client: ${clientResult.error}');
      return clientResult.errorCast();
    }

    final client = clientResult.value;
    final result = await client.getGalleryData();
    if (result.isFailure) {
      return result.errorCast();
    }

    for (final gallery in result.value) {
      _galleries[gallery.name] = gallery;
    }

    _loaded = true;
    notifyListeners();
    return Result.ok(_galleries.values.toList(growable: false));
  }

  Result<List<ImageEntry>, ExecError> getGalleryImageList(String galleryName) {
    if (!_galleries.containsKey(galleryName)) {
      return Result.error(
        ExecError(
          type: ErrorType.localError,
          message: 'Unknown Gallery: $galleryName',
        ),
      );
    }

    if (!_images.containsKey(galleryName)) {
      updateGalleryImageList(galleryName);
      return Result.ok([]);
    } else {
      return Result.ok(_images[galleryName]!.toList(growable: false));
    }
  }

  //TODO Paging
  FutureResult<List<ImageEntry>, ExecError> updateGalleryImageList(
      String galleryName) async {
    final auth = getIt<AuthService>();
    final clientResult = auth.currentClient;
    if (clientResult.isFailure) {
      _logger.severe('Error getting Friendica client: ${clientResult.error}');
      return clientResult.errorCast();
    }

    final client = clientResult.value;
    final result = await client.getGalleryImages(galleryName);
    if (result.isFailure) {
      return result.errorCast();
    }

    final imageSet = _images.putIfAbsent(galleryName, () => {});
    imageSet.addAll(result.value);

    notifyListeners();
    return Result.ok(imageSet.toList(growable: false));
  }
}
