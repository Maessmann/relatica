import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../globals.dart';
import '../../models/connection.dart';
import '../../models/timeline_entry.dart';
import '../../routes.dart';
import '../../services/connections_manager.dart';
import '../../utils/dateutils.dart';
import '../image_control.dart';
import '../padding.dart';

class StatusHeaderControl extends StatelessWidget {
  final TimelineEntry entry;

  const StatusHeaderControl({
    super.key,
    required this.entry,
  });

  void goToProfile(BuildContext context, String id) {
    context.pushNamed(ScreenPaths.userProfile, params: {'id': id});
  }

  @override
  Widget build(BuildContext context) {
    final author = getIt<ConnectionsManager>()
        .getById(entry.authorId)
        .getValueOrElse(() => Connection());

    final reshareAuthor = getIt<ConnectionsManager>()
        .getById(entry.reshareAuthorId)
        .getValueOrElse(() => Connection());
    return Wrap(
      children: [
        ImageControl(
          imageUrl: author.avatarUrl.toString(),
          iconOverride: Icon(Icons.person),
          width: 32.0,
          onTap: () => goToProfile(context, author.id),
        ),
        const HorizontalPadding(),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => goToProfile(context, author.id),
              child: Text(
                author.name,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  ElapsedDateUtils.epochSecondsToString(
                      entry.backdatedTimestamp),
                  style: Theme.of(context).textTheme.caption,
                ),
                const HorizontalPadding(),
                Icon(
                  entry.isPublic ? Icons.public : Icons.lock,
                  color: Theme.of(context).hintColor,
                  size: Theme.of(context).textTheme.caption?.fontSize,
                ),
              ],
            ),
            // Text(
            //   entry.id,
            // ),
          ],
        ),
        if (reshareAuthor.isNotEmpty) ...[
          const HorizontalPadding(width: 3.0),
          const Icon(Icons.repeat),
          const HorizontalPadding(width: 3.0),
          ImageControl(
            imageUrl: reshareAuthor.avatarUrl.toString(),
            iconOverride: Icon(Icons.person),
            width: 32.0,
            onTap: () => goToProfile(context, reshareAuthor.id),
          ),
          const HorizontalPadding(width: 3.0),
          GestureDetector(
            onTap: () => goToProfile(context, reshareAuthor.id),
            child: Text(
              reshareAuthor.name,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ],
      ],
    );
  }
}
