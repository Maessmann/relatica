import 'package:flutter/material.dart';

class VerticalPadding extends StatelessWidget {
  static const defaultHeight = 20.0;
  final double height;

  const VerticalPadding({super.key, this.height = defaultHeight});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
    );
  }
}

class HorizontalPadding extends StatelessWidget {
  static const defaultWidth = 10.0;
  final double width;

  const HorizontalPadding({super.key, this.width = defaultWidth});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
    );
  }
}
