import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../globals.dart';
import '../models/attachment_media_type_enum.dart';
import '../models/media_attachment.dart';
import '../screens/image_viewer_screen.dart';
import '../utils/snackbar_builder.dart';
import 'image_control.dart';
import 'video_control.dart';

class MediaAttachmentViewerControl extends StatefulWidget {
  final MediaAttachment attachment;

  const MediaAttachmentViewerControl({super.key, required this.attachment});

  @override
  State<MediaAttachmentViewerControl> createState() =>
      _MediaAttachmentViewerControlState();
}

class _MediaAttachmentViewerControlState
    extends State<MediaAttachmentViewerControl> {
  @override
  Widget build(BuildContext context) {
    final item = widget.attachment;
    const width = 250.0;
    const height = 250.0;
    if (item.explicitType == AttachmentMediaType.video) {
      if (useVideoPlayer) {
        return VideoControl(
          videoUrl: widget.attachment.uri.toString(),
          width: width,
          height: height,
        );
      }
      return GestureDetector(
          onTap: () async {
            final confirm = await showYesNoDialog(
                context, 'Open Video Link in external app? ${item.uri}');
            if (confirm != true) {
              return;
            }
            if (await canLaunchUrl(item.uri)) {
              if (mounted) {
                buildSnackbar(
                  context,
                  'Attempting to launch video: ${item.uri}',
                );
              }
              await launchUrl(item.uri);
            } else {
              if (mounted) {
                buildSnackbar(context, 'Unable to launch video: ${item.uri}');
              }
            }
          },
          child: SizedBox(
            height: height,
            width: width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      item.description.isNotEmpty
                          ? item.description
                          : 'Video: ${item.uri}',
                      softWrap: true,
                    ),
                  ),
                ),
              ],
            ),
          ));
    }
    if (item.explicitType != AttachmentMediaType.image) {
      return Text('${item.explicitType}: ${item.uri}');
    }

    return ImageControl(
      width: width,
      height: height,
      imageUrl: item.thumbnailUri.toString(),
      altText: item.description,
      onTap: () async {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return ImageViewerScreen(attachment: item);
        }));
      },
    );
  }
}
