import 'package:flutter/material.dart';

import '../routes.dart';

class StandardAppBar {
  static AppBar build(BuildContext context, String title) {
    return AppBar(
      title: Text(title),
      actions: [
        IconButton(
          onPressed: () {
            Navigator.of(context).popUntil((route) {
              return route.settings.name == ScreenPaths.timelines;
            });
          },
          icon: const Icon(Icons.home),
        ),
      ],
    );
  }
}
