import 'package:result_monad/result_monad.dart';

import '../../globals.dart';
import '../../models/connection.dart';
import '../../models/exec_error.dart';
import '../../objectbox.g.dart';
import '../interfaces/connections_repo_intf.dart';
import '../memory/memory_connections_repo.dart';
import 'objectbox_cache.dart';

class ObjectBoxConnectionsRepo implements IConnectionsRepo {
  late final Box<Connection> box;
  final memCache = MemoryConnectionsRepo();

  ObjectBoxConnectionsRepo() {
    box = getIt<ObjectBoxCache>().store.box<Connection>();
  }

  @override
  bool addAllConnections(Iterable<Connection> newConnections) {
    memCache.addAllConnections(newConnections);
    final result = box.putMany(newConnections.toList());
    return result.length == newConnections.length;
  }

  @override
  bool addConnection(Connection connection) {
    memCache.addConnection(connection);
    box.putAsync(connection);
    return true;
  }

  @override
  Result<Connection, ExecError> getById(String id) {
    final result = memCache.getById(id);
    if (result.isSuccess) {
      return result;
    }

    return _getConnection(
      Connection_.id.equals(id),
    ).mapError(
      (error) => error.copy(
        message: "Can't find Connection for ID: $id",
      ),
    );
  }

  @override
  Result<Connection, ExecError> getByName(String name) {
    final result = memCache.getByName(name);
    if (result.isSuccess) {
      return result;
    }
    return _getConnection(
      Connection_.name.equals(name),
    ).mapError(
      (error) => error.copy(
        message: "Can't find Connection for ID: $name",
      ),
    );
  }

  @override
  List<Connection> getKnownUsersByName(String name) {
    return box
        .query(
          Connection_.name.contains(name, caseSensitive: false) |
              Connection_.handle.contains(name, caseSensitive: false),
        )
        .build()
        .find();
  }

  @override
  List<Connection> getMyContacts() {
    final connectedStatuses = [
      ConnectionStatus.youFollowThem.code,
      ConnectionStatus.theyFollowYou.code,
      ConnectionStatus.mutual.code,
    ];
    return box
        .query(Connection_.dbStatus.oneOf(connectedStatuses))
        .build()
        .find();
  }

  @override
  bool updateConnection(Connection connection) {
    memCache.updateConnection(connection);
    box.put(connection);
    return true;
  }

  Result<Connection, ExecError> _getConnection(Condition<Connection> query) {
    final connection = box.query(query).build().findFirst();
    if (connection == null) {
      return buildErrorResult(type: ErrorType.notFound);
    }

    memCache.addConnection(connection);
    return Result.ok(connection);
  }
}
