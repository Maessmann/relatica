import 'package:result_monad/result_monad.dart';

import '../../models/exec_error.dart';
import '../../models/group_data.dart';

class IGroupsRepo {
  void addAllGroups(List<GroupData> groups) {
    throw UnimplementedError();
  }

  void clearGroups() {
    throw UnimplementedError();
  }

  List<GroupData> getMyGroups() {
    throw UnimplementedError();
  }

  Result<List<GroupData>, ExecError> getGroupsForUser(String id) {
    throw UnimplementedError();
  }

  bool updateConnectionGroupData(String id, List<GroupData> currentGroups) {
    throw UnimplementedError();
  }
}
