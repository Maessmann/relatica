import 'package:result_monad/result_monad.dart';

import '../../models/exec_error.dart';
import '../../models/group_data.dart';
import '../interfaces/groups_repo.intf.dart';

class MemoryGroupsRepo implements IGroupsRepo {
  final _groupsForConnection = <String, List<GroupData>>{};
  final _myGroups = <GroupData>{};

  @override
  Result<List<GroupData>, ExecError> getGroupsForUser(String id) {
    if (!_groupsForConnection.containsKey(id)) {
      return Result.error(ExecError(
        type: ErrorType.notFound,
        message: '$id not a known user ID',
      ));
    }

    return Result.ok(_groupsForConnection[id]!);
  }

  @override
  List<GroupData> getMyGroups() {
    return _myGroups.toList();
  }

  @override
  void clearGroups() {
    _myGroups.clear();
  }

  @override
  void addAllGroups(List<GroupData> groups) {
    _myGroups.addAll(groups);
  }

  @override
  bool updateConnectionGroupData(String id, List<GroupData> currentGroups) {
    _groupsForConnection[id] = currentGroups;
    return true;
  }
}
