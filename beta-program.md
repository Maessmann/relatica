# Relatica Beta Testing Program Notes

These are notes for the Relatica beta testers. Very import sections of this page
to check are:

* ["Things that work"](#things-that-work) to see what is working well
* ["Things that are broken"](#broken-and-hopefully-fixed-in-the-very-near-future-) so
  you know how to avoid problems using the application in its current state,
  don't report duplicate bugs, etc.
* ["Cumbersome things"](#cumbersome-and-hopefully-improved-in-the-very-near-future-) 
  for the same reason as the above "broken" things.
* [The CHANGELOG](CHANGELOG.md) to see what has changed with each recent version
* [Install Instructions](install.md) to see how to install the application
* [Community Communications](README.md#community-and-support) to know how to
  get in touch with other members of the community, developers, etc. 

## Introduction 
At the present time it is possible to use Relatica as your daily driver for
functions that have already been implemented. I've been using it as my 
primary Friendica interactions for over a month as of writing this in
January 2023. However, usable doesn't mean feature complete, even for the
subset of features I want to implement. Usable also doesn't mean bug free
experience. It can still be useful to people which is why I'm opening up
access to it right now. Having said that, this is an early beta program. 
Since not everyone that is considering using the software at this point may
be familiar with that aspect of the software development process I 
wanted to lay out some expectations before getting into the small details

### Expectations For Early Beta Testers
* Things that look like they are finished are not
* A lot of things look very not finished
* Download regular updates to get the latest and greatest features and fixes and feedback
* Check in on the CHANGELOG, Issue tracker, etc. for updates
* Some things that look finished may end up changing to improve overall user experience (the "Who moved the gas pedal?" problem)
* Don't suffer in silence with problems. The beta program is about getting the feedback to what works, what doesn't, etc.
  Provide feedback via the Matrix room, for interactive discussions, or the GitLab issue tracker for larger bug tracking.
* Feedback should be productive and specific while being as light a load on the beta tester as possible:
    * BAD: "This thing is crap!"
    * BAD: "It crashed!!!!"
    * Good: "When I click on a notification box it didn't seem to do anything until I clicked in a very specific area"
    * Good: "The app seemed to freeze up when I kept scrolling down my timeline for a long time (like half an hour)"
    * Good: "I'm finding X feature hard to use because Y is confusing"
    * Good: "I am trying to do X with Friendica but I don't see a way to do it. Could that be implemented in the app?"

## Application Status and Roadmap

### Things that work
* Logging in with username/password. These are stored using the OS specific key vaults
* Writing posts
    * Typing @ brings up a list of all known fediverse accounts that the app has ever seen as you type (but not all that your server has seen)
    * Typing # brings up a list of all known hashtags that the app has ever seen as you type (but not all that your server has seen)
    * Very basic markdown (wrapping in stars for bold/italics)
    * Adding new images to posts 
      * Image posting with easy means of adding ALT text 
    * Attaching existing images to posts
* Resharing posts
* Browsing timelines including from groups you've created. In this app "Home" is the same as the "Network" (grid) button in Frio and "Yours" is the equivalent of the Home button in the app.
* Browsing notifications and marking individual ones read or all read
* Getting gallery list and browsing galleries
* Browsing your contacts or searching contacts that are known by your app
* Adjudicating follow requests
* Going to individual profiles to see their posts, follow/unfollow, add/remove from groups
* A "low bandwidth" mode that defers loading of images until you click on them, unless the app already has that image cached locally.
* Light/dark mode selection
* Opening posts, comments, and profiles in the "external" browser
* Playing videos within the app on iOS and Android.
* Pulling down to refresh timelines, galleries, and posts but loading newer or older posts has specific buttons (this will probably change in the near future)
* Pulling down to refresh notifications and contacts gets updates to that respective data (this may change in the near future)

### Big things I want to have working in the near future:
* Show list of who liked and reshared posts/comments
* Better timeline UX to allow for "in-fill"
* More detailed profile information for users and logged in user
* Better data display on larger format displays by doing things like:
    * Allowing images/thumbnails to be larger
    * Limiting maximum width of timeline columns
* Nicer aesthetics
* Fix below identified issues
* Private messages
* Notifications broken into:
    * Messages (from message system)
    * Friend requests
    * Real notifications
* Fixing the image scaling in views on desktop
* More greedy loading of notifications when not in low bandwidth mode
* More intuitive timeline controls
* Real paging across the board
    * Notifications
    * Comments
    * Galleries
    * Contacts
* Copying text, captions, etc.
* Easier copying link to post/comment
* Offline caching of older content
* Dynamic search of locally known posts and hashtags.
* Make forum posts
* Internationalization
* Better screen reader interactions


### Further out things I want to have working:
* Post editing
* Profile editor
* Group creation, deletion, renaming
* Being able to load accounts and/or posts from links from other users within app
* Open fediverse accounts/files within app
* Nitter replacement of Twitter links
* User configurable Server blocking
* Server-side searching tied into profiles, posts, hashtags
* OAuth logins
* Being able to ignore/unignore users
* Deleting images and entire galleries
* Events


### Things I don't envision implementing in the foreseeable future:
* Account creation through the application
* Creating new forums through the application
* Site administration
* Two-factor authentication
* Adding videos or files to posts
* Multi-account logins


## General behaviors/problems to be aware of:

### Broken and hopefully fixed in the very near future:
* Resharing of Diaspora federated posts is currently broken server side. All other posts should be reshareable.
* Content Warnings/Spoiler Text on *posts* aren't federating over to Mastodon well so only use it on Comments for now
* ALT text on images should not have any quotation marks as it breaks when federating over to Diaspora for the time being
* Portrait videos overflow their boxes in the timeline
* Blocked/ignored user's content is still returned by the API
* Paging for some of the endpoints either isn't wired in yet or is not working as needed server side. That includes things like:
    * Friend requests
    * Connections list
    * Gallery Contents
    * Comments on posts
    * Tags
    * Notifications
    * Blocked user list

### Cumbersome and hopefully improved in the very near future:
* Notifications need to be manually refreshed.
* The tap zone for some things is not always intuitive. For example on notifications clicking on white space around it doesn't count as a hit but you have to hit the non-hyperlinked text.
* Responsiveness can be laggy. Sometimes hitting buttons doesn't seem to do something but it is doing a network request. I know I need to improve that
* In galleries you need to double click on the picture to open the preview. It is remnants of an experiment I was doing on more consistent UX which feels broken so I'm changing.
* Sometimes timelines get confused so swapping between the different groups/timelines creates a muddled display. Restarting the app fixes this.
* Image zoom is broken on desktops that use mouse wheels for scrolling. It only does full or no zoom.
* Image zoom can get stuck on mobile when zoomed in too far. Backing out of the image to the gallery and back in fixes it
* Some images within posts, usually graphical emojis, are rendered drastically larger than they should be.
* On Linux you will need to re-enter your credentials each time you use the app for the first time after logging in.
* Groups are listed by the order they were created not alphabetically in the drop down menu
* Liking a nested comment can appear to lock up (it stays grayed out). Navigating back and forth fixes that
* The "in fill" problem: Timelines fill only at the ends with at most 20 posts per call. So let's say you logged in at 09:00 and the initial pulls went from 07:00 to 09:00:
  ``` 
  07:00=====9:00
  ```
  
  While browsing you asked for older posts that pulled posts back to 06:00 and up to the time you asked for more, say 09:30 so the timeline looks like:
  ```
  06:00=========09:30
  ```
  Now at lunch time you come back and ask for newer posts. It will pull the last 20 posts from 12:00 which say took you back to 11:00.
  The timeline looks contiguous but it really is:
  ```
  06:00=========09:30          11:00=====12:00
  ```
  There may be posts in the 09:30 to 11:00 gap or may not. Right now there is no way to fill
